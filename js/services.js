angular.module('zasta.services', [])
.value('buildNumber', 32)
.factory('Password', function($timeout, Accounts, passwordTimeout) {
  var sessionPassword = false;

  function askForPassword($ionicPopup, $scope, callback) {
    $scope.decrypt = {};
    $ionicPopup.prompt({
      title: 'Passwort für Verschlüsselung',
      subTitle: 'Bitte dein Passwort eingeben',
      inputType: 'password'
    }).then(function(password) {
      try {
        Accounts.load(password);
      } catch(e) {
        callback(e, null);
        return;
      }
      $scope.decrypt = {};
      sessionPassword = password;
      callback(false, password);
    });

    $timeout(function() {
      sessionPassword = false;
    }, passwordTimeout);
  }

  return {
    withPassword: function($ionicPopup, $scope, callback) {
      if(!sessionPassword) return askForPassword($ionicPopup, $scope, callback);
      callback(false, sessionPassword);
    }
  };
})
.factory('Accounts', function($rootScope) {
  var accounts = [], loaded = false;

  function loadAccounts(password) {
    var cipheredData = localStorage.getItem("accounts");
    if(!cipheredData) {
      accounts = [];
      loaded = true;
      return;
    }

    try {
      var plainData = sjcl.decrypt(password, cipheredData);
    } catch (e) {
      throw 'Das Passwort war leider falsch :(';
    }

    try {
      accounts = JSON.parse(decodeURIComponent(plainData)) || [];
    } catch(e) {
      accounts = [];
    }
    loaded = true;
  }

  var storeAccounts = function(password, accounts) {
    // check if the password is okay
    try{
      if(!localStorage.getItem('accounts') || sjcl.decrypt(password, localStorage.getItem('accounts'))) {
        localStorage.setItem("accounts", sjcl.encrypt(password, encodeURIComponent(JSON.stringify(accounts)), {ks: 256}));
      }
    } catch(e) {
      throw 'Das Passwort war leider falsch :(';
    }
  }

  var updateBank = function(bank, onReady) {
    if(!BANKS[bank.blz]) {
      Bugsense.notify(new Error('UNKNOWN BANK'), {blz: bank.blz});
      return onReady("BLZ unbekannt");
    }

    try {
      Hbci.getKontostand({
        loginId: bank.user,
        pin: bank.pin
      }, {
        blz: bank.blz,
        url: BANKS[bank.blz].url,
        version: BANKS[bank.blz].version || "3.0"
      }, function(err, balances) {
        if(err) {
          console.log("Error:", err);
          onReady(err);
          return;
        }
        onReady(false, balances);
      });
    } catch(e) {
      console.log("Exception", e);
      var info = {};
      if(bank) {
        info.blz  = bank.blz;
        info.bank = BANKS[bank.blz];
      }
      Bugsense.notify(e, info);
      onReady(e);
    }
  };

  var self = {
    loaded: function() { return loaded; },
    load: loadAccounts,
    save: storeAccounts,
    all: function() {
      return accounts;
    },
    get: function(index) {
      return accounts[index];
    },
    getByType: function(type) {
      return accounts.filter(function(acc) { return acc.type == type; });
    },
    update: function(onReady) {
      var iBank = accounts.length - 1;
      do {
        if(accounts[iBank].type !== 'Bank') continue;
        (function(arrayIndex, bankIndex) {
          updateBank(accounts[arrayIndex], function(err, balances) {
            if(err) {
              console.log("Error:", err);
              accounts[arrayIndex].hasError = true;
              accounts[arrayIndex].error = err;
              return;
            } else {
              accounts[arrayIndex].hasError = false;
              accounts[arrayIndex].error = undefined;
            }

            for(var b=0, bLen = balances.length; b<bLen; b++) {
              var balanceAcc = balances[b], found = false;
              for(var a=0, aLen = accounts.length; a<aLen; a++) {
                if(accounts[a].type != "Account") continue;
                if(accounts[a].name == balanceAcc.account && accounts[a].bank == bankIndex) {
                  // Update existing account
                  accounts[a].balance = balanceAcc.balance;
                  found = true;
                  break;
                }
              }

              if(!found) {
                //Add new account
                accounts.splice(arrayIndex+1, 0, {
                  type: 'Account',
                  name: balances[b].account,
                  balance: balances[b].balance,
                  bank: bankIndex
                });
              }
            }
            onReady(false, accounts);
          });
        })(iBank, accounts[iBank].bankIndex);
      } while(iBank--);
    },
    add: function(bank, onReady) {
      if(!BANKS[bank.blz]) {
        Bugsense.notify(new Error('UNKNOWN BANK'), {blz: bank.blz});
        return onReady("BLZ unbekannt");
      }

      var bankIndex = self.getByType('Bank').length;
      bank.type = 'Bank';
      bank.name = BANKS[bank.blz].name;
      bank.bankIndex = bankIndex;
      updateBank(bank, function(err, balances) {
        if(err) {
          console.log("Error:", err);
          onReady(err);
          return;
        }
        accounts.push(bank);
        for(var i=0, len = balances.length; i<len; i++) {
          accounts.push({ type: 'Account', name: balances[i].account, balance: balances[i].balance, bank: bankIndex });
        }

        onReady(false, accounts);
      });
    },
    remove: function(bankIndex) {
      var len = accounts.length;
      while(len--) {
        if(accounts[len].bankIndex == bankIndex || accounts[len].bank == bankIndex) {
          accounts.splice(len, 1);
        }
      }
      return self.getByType('Bank');
    }
  };

  return self;
});
