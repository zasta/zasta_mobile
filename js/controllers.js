angular.module('zasta.controllers', [])

.controller('DashCtrl', function($scope, $timeout, $ionicModal, $ionicPopup, $location, Accounts, Password) {
  if(!localStorage.getItem("setupWasDone")) {
    $ionicModal.fromTemplateUrl('modal-welcome.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });

    $scope.go = function() {
      localStorage.setItem("setupWasDone", true);
      $scope.modal.hide();
      $location.path('/tab/accounts/new');
    }
    $scope.$on('$destroy', function() { $scope.modal.remove(); })
    return;
  }
  if(!Accounts.loaded()) {
    (function tryGetPassword() {
      Password.withPassword($ionicPopup, $scope, function(err, password) {
        if(err) {
          if(confirm("Hoppla! " + err + " - Erneut versuchen?")) { tryGetPassword(); }
          return;
        }
        Accounts.load(password);
        $scope.accounts = Accounts.all();
      });
    })();
  } else {
    $scope.accounts = Accounts.all();
  }

  $scope.refresh = function() {
    $scope.refreshing = true;
    (function tryGetPassword() {
      Password.withPassword($ionicPopup, $scope, function(err, password) {
        if(err) {
          if(confirm("Hoppla! " + err + " - Erneut versuchen?")) { tryGetPassword(); }
          return;
        }
        
        Accounts.update(function(err, accounts) {
          if(err == false) {
            Accounts.save(password, $scope.accounts);
          }
  
          $timeout(function() {
            $scope.refreshing = false;
            if(err) {
              alert(err);
            }
            $scope.accounts = accounts;
          });
        });
      });
    })();
  };
})
.controller('AccountsCtrl', function($scope, $ionicPopup, Accounts, Password) {
  $scope.accounts = Accounts.getByType('Bank');
  $scope.delete = function(bankIndex) {
    $scope.accounts = Accounts.remove(bankIndex);
    (function tryGetPassword() {
      Password.withPassword($ionicPopup, $scope, function(err, password) {
        if(err) {
          if(err) {
            if(confirm("Hoppla! " + err + " - Erneut versuchen?")) { tryGetPassword(); }
            return;
          }
          return
        }
        Accounts.save(password, $scope.accounts);
      });
    })();
  }
})
.controller('AccountAddCtrl', function($scope, $ionicPopup, $location, $timeout, Accounts, Password) {
  $scope.bank = {};
  $scope.loading = false;

  $scope.addBank = function() {
    $scope.error = false;
    $scope.loading = true;
    Accounts.add($scope.bank, function(err, accounts) {
      $timeout(function() {
        $scope.loading = false;
        if(err) {
          $scope.error = err;
        } else {
          (function tryGetPassword() {
            Password.withPassword($ionicPopup, $scope, function(err, password) {
              if(err) {
                if(confirm("Hoppla! " + err + " - Erneut versuchen?")) { tryGetPassword(); }
                return;
              }
              Accounts.save(password, accounts);
            });
          })();
          $location.path('/tab/accounts');
        }
      });
    });
  }
})
.controller('SettingsCtrl', function($scope, buildNumber) {
  $scope.buildNumber = buildNumber;
});
